---
title: About
layout: layouts/index-page.njk
---

# Jordan Shurmer

I am a Christian, a husband, and a dad. By trade, I am a software engineer.

This website is a place for me to try out new web technology, and a place to keep/organize some of my thoughts.


Twitter: [@jshurmer](https://twitter.com/jshurmer)
<br>
Github:  [JordanShurmer](https://github.com/JordanShurmer)