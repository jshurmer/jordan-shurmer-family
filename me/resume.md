---
title: Resume
layout: layouts/very-basic.njk
---

Jordan E. Shurmer
=================

Software Skills
---------------

Full Stack Web Development | JavaScript | HTML | CSS | Java | Python | Rust-Lang | Powershell | TCL | SQL | Bash | Perl | Test Driven Development

Education
---------

**Dec. 2018 - University of Tennessee Knoxville**<br>
_B.S. in Computer Science_

Work Experience
---------------

### Warner Bros. Discover

Senior Software Engineer<br>
_August 2012 - Present_<br>



* Full Stack web development for [HGTV.com](https://www.hgtv.com/), [FoodNetwork.com](https://www.foodnetwork.com/), [TravelChannel.com](https://www.travelchannel.com/), [Food.com](https://www.food.com) and other production websites
    * Frontend apps using jQuery, Vue, t3-js, SvelteKit, HTMX, and vanilla JS
    * Back end servers using Java (Apache Sling / AEM), Express, SvelteKit, Python, and Rust
* Developed APIs, internal tools, and build systems supporting those websites in multiple languages (Java, python, groovy, ruby, javascript, bash, Maven, Gradle, Make, NPM/Yarn, Webpack, Vite, Make, AWS SAM)
* Championed the use of TDD patterns across various software development teams
* Debugged production websites using production log files, recreating the problem on a dev environment, using a live debugger, analyzing the codebase, etc.
* Developed custom authoring experiences within an existing CMS
* Participated in every aspect of a complete product lifecycle including gathering requirements, discovery sessions with users, proposing solutions, implementation, peer reviews, quality assurance, deployment, maintenance, and sunsetting

<style>

</style>