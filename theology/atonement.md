---
title: Atonement
headline: The Atonement
description: "A look into the Atonement: the most glorious of the christian doctrines, the crux of christianity"
date: 2019-02-20
templateEngine: njk,md
---
{% import "macros/scripture.njk" as scripture %}


## Introduction

The subject of the atonement is all about what happened on the cross. What did Jesus's death accomplish? 

This is a glorious subject, which we can only know in part.
* Paul prays in Ephesians for the church to <q>know the love of Christ that surpasses knowledge</q>
* God's love is defined in terms of the atonement
    * 1 John: this is love, not that we have loved God but that he loves us and sent his Son to be the propitiation for our sins
    * Romans 5:8: God shows his love for us in that while we were yet sinners Christ died for us
    * John 3:16: God so loved the world, that he gave his only Son, that whoever believes in him should not perish by have eternal life
* Ephesians 2 says that God saved us **so that** in the coming ages he might show us the immeasurable riches of his grace in kindness towards us in Christ Jesus
* 1 Peter describes the sufferings of Christ and subsequent glories as <q>things into which angels long to look.</q>


So, what happened on the Cross? This is the love of God revealed. This is a subject we will be looking into for all eternity. This is a subject that the angels, who dwell with God, are eager to look into.

There are various ways of describing what God accomplished on the Cross. Apart from this work we are in **bondage** to sin, **separated** from God, and deserving his **wrath**. The atonement provides the solution as **redemption**, **reconciliation**, and **propitiation**.

## Propitiation

Jesus was a sacrifice of propitiation. This means his sacrifice removes the wrath of God from us and brings the favor of God to us. The gospel is that God is now _propitious_ (favorable) towards us through the sacrifice of Jesus in the place of his people. He bore the wrath that I deserve and I am credited with his favor in God's sight.

{{ scripture.list([
  "Romans 3:21-26",
  "Hebrews 2:14-18",
  "1 John 4:7-11"
  ]) }}

### Wrath Removal

The wrath of God that I deserve was poured out on the Song of God instead. And the righteousness of Christ is credited to me.

The cup of God's wrath (Jeremiah 25:15, Isaiah 51:17, Revelation 14:10) was poured out on Jesus. In the garden of gethsemane, the night he was betrayed, Jesus prayed multiple times "My Father, if it be possible, let this cup pass from me; nevertheless, not as I will but as you will".

{{ scripture.list([
  "Isaiah 53:4-6",
  "Galatians 3:13",
  "Colossians 2:14",
  "1 Peter 2:24-25",
  "Hebrews 9:24-28"
  ]) }}

### Righteousness Granted

Not only is God's wrath removed, but Jesus's righteousness is now mine!

{{ scripture.list([
  "Galatians 2:20-21",
  "2 Corinthians 5:21",
  "Romans 5:19",
  "Philippians 3:9"
  ]) }}

## Reconciliation

I was once separated from God due to my Sin and his Holiness. Only through the work of Christ's sacrifice on the cross am I reconciled to God.

{{ scripture.list([
      "2 Corinthians 5:18-21",
      "Romans 5:10-11",
      "Ephesians 2:13-16"
  ]) }}

## Redemption

I was in bondage to my sin and to Satan, and needed someone to provide redemption in order to redeem me from that bondage.

{{ scripture.list([
  "1 Peter 1:18-20",
  "Mark 10:45",
  "Acts 20:28",
  "Hebrews 2:14-15",
  "Colossians 1:13-14"
  ]) }}
 
## Resources

These are some resources I have found helpful on the topic of the atonement

### Online

* J.I. Packer, Mark Dever: [In My Place Condemned He Stood](https://www.wtsbooks.com/products/in-my-place-condemned-he-stood-j-i-mark-dever-packer-9781433502002?variant=9747692847151)
* C.H. Spurgeon: [Christ's Finished Work](https://www.spurgeon.org/resource-library/sermons/christs-finished-work)
* C.H. Spurgeon: [Christ Crucified](http://www.spurgeongems.org/vols46-48/chs2673.pdf)
* Paul Washer (audio): [Penal Substitutionary Atonement, The Pinnacle of God's Saving Plan](https://www.sermonaudio.com/sermoninfo.asp?SID=12019115526004)
* [Monergism's Atonement Index](https://www.monergism.com/topics/atonement)
* DesiringGod.org - [The Death of Christ](https://www.desiringgod.org/topics/the-death-of-christ/messages?sort=oldest)

### Books

* R.C. Sproul: [The Truth of the Cross](https://www.wtsbooks.com/products/the-truth-of-the-cross-r-c-sproul-9781567690873)
* D.A. Carson: [Scandalous: The Cross and the Resurrection of Jesus](https://www.wtsbooks.com/products/scandalous-d-a-carson-9781433511257?variant=9747855540271)
* John Owen: [The Death of Death in the Death of Christ](https://www.wtsbooks.com/products/death-death-death-christ-john-owen-9781781919064?variant=9796377575471) ([ebook/pdf](https://www.monergism.com/death-death-death-christ-ebook)) ([web](http://www.monergism.com/thethreshold/sdg/owen_death_index.htm))

### Music

* Shai Linne: [The Atonement](https://open.spotify.com/album/5f1Wt873Rlq3UkK5eP0zp7?si=WsU6VcLBQ2OTz91SemWRnA)
* SGM: [Grace and Peace](https://sovereigngracemusic.org/music/songs/grace-and-peace/)
* SGM: [Hallelujaj, What a Savior](https://sovereigngracemusic.org/music/songs/hallelujah-what-a-savior/)
* SGM: [O My Soul, Arise](https://sovereigngracemusic.org/music/songs/o-my-soul-arise/)
* William Cowper: [There is a Fountain Filled with Blood](https://sovereigngracemusic.org/music/songs/there-is-a-fountain-filled-with-blood/)
* SGM: [What a Savior](https://sovereigngracemusic.org/music/songs/what-a-savior/)
