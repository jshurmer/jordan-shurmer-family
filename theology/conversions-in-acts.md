---
title: Conversions in Acts
date: 2020-06-21
description: "List of conversions in the book of Acts"
templateEngine: njk,md
---

{% import "macros/scripture.njk" as scripture %}


{{ scripture.list([
    "Acts 2:37–41",
    "Acts 3:19–20, 4:4",
    "Acts 5:12–14",
    "Acts 6:7",
    "Acts 8:12–13",
    "Acts 8:35–38",
    "Acts 9:17–19",
    "Acts 9:31",
    "Acts 9:32–35",
    "Acts 9:40–42",
    "Acts 10:43–48, 11:13-18,15:7–11",
    "Acts 11:21–24",
    "Acts 13:7–12",
    "Acts 13:38,43",
    "Acts 13:47–49",
    "Acts 14:1",
    "Acts 16:4–5",
    "Acts 16:13–15",
    "Acts 16:29–34",
    "Acts 17:2–4",
    "Acts 17:10–12",
    "Acts 17:32–34",
    "Acts 18:8"
]) }}