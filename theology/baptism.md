---
title: Baptism
date: 2019-01-30
description: "A look into my views on baptism. In particular, a response to the Churches of Christ understanding of baptism."
templateEngine: njk,md
---

{% import "macros/scripture.njk" as scripture %}

<section>

## Baptism is Symbolic

<p class="summary">
I believe baptism is a physical <strong>symbol</strong> of what happens at conversion
</p>

The English word _baptism_ is a transliteration of the greek word being used, rather than a translation. The word means simply _immersion_. So, what the original hearers and readers of all of the things said about baptism heard was to be _immersed in Christ_, _immersed in the name of Jesus Christ_, etc. I believe they would have easily understood the apostles to mean that baptism is a symbol for what actually happens in conversion, not that baptism is the means to bring about conversion. In conversion we are united to and immersed in Christ and his burial and resurrection and our sins are washed from us - those things are symbolized by being immersed in water in the name of Jesus.

Reading _baptism_ as _immersion_ helps to see the clearly symbolic usage, since _immerse_ is used symbolically in general. For example, we speak of being immersed in our work, immersed in music, etc. The new testament uses the word similarly: immersed with/in the Holy Spirit (Matthew 3:11, Mark 1:8, Luke 3:16, John 1:33, Acts 1:5), immersed in suffering (Mark 10:38, Luke 12:50), immersed into death (Romans 6:3), immersed into Moses (1 Corinthians 10:2), immersed into the church (1 Corinthians 12:13).


So, when reading things like 1 Peter 4:21 _baptism now saves you_, and Acts 2:38 _Repent and be baptized for the forgiveness of your sins_, this can easily be understood symbolically. The act of baptism doesn't literally save you - it doesn't bring about salvation - it's a symbol of what saves you (buried and raised with Christ through faith). In the same way we might say _this wedding ring unites us_, or _I pledge allegiance to the flag_,  even though those symbols aren't the reality that they symbolize. Symbols are meant to be used in place of the thing they symbolize. It does makes sense to say _baptism saves you_ when baptism is symbolic of our union with Christ (which is actually accomplished through faith, not immersion in water).

</section>

<section>

## Baptism in Salvation


Why I don't think baptism is the entry point into Christianity. (A response to the [Churches of Christ doctrine](https://en.wikipedia.org/wiki/Churches_of_Christ#Baptism))

I believe that baptism is not the entry point into salvation/Christianity, faith is. Justification happens the moment you hear the gospel and believe, not the moment you are immersed in a baptismal pool. 1 Peter 4:21, Mark 16:16, Acts 2:38, Romans 6:3, Galatians 3:27 and more, all point to baptism being essential and necessary for salvation, so why would I not just believe the most literal interpretation of those verses? Simply, because the most literal interpretation contradicts the clear teaching that justification is by faith alone. The moment you truly believe the gospel, trust Jesus for the forgiveness of your sins and righteousness through him, treasure God as the means of joy and satisfaction, turn from your sin and self-righteousness and rely on God, you are counted righteous is God's eyes and united to Christ.

</section>

<aside id="on-faith">

### On Faith

What faith? Faith in what?

"Now faith is the assurance of things hoped for, the conviction of things not seen." (Hebrews 11:1). When I say that we are justified by faith alone what I mean is that we are justified by the assurance of things hoped for and the conviction of things not seen. The assurance and conviction of _forgiveness through the death of Jesus on my behalf, bearing the penalty of my sins_ (something not seen and hoped for); the assurance and conviction _of God counting me righteous because of the work of Christ_ (something not seen and hoped for); the assurance and conviction that _Jesus was made to be my sin and became a curse for me so that I would be righteous_ (something not seen and hoped for); etc. This definition of faith is not compatible with one which includes an act (such as baptism) in the very definition of what faith is. Actions flow out of faith, but they are not the definition of faith.

I will note that this is _justification_ by faith alone, not final salvation by faith alone. Final salvation is only for those who persevere in this faith, working it out while God is at work in them, etc. For more on what I mean by this see <a href="https://www.desiringgod.org/articles/does-god-really-save-us-by-faith-alone">this excellent article</a>. On this point I agree with the Churches of Christ's criticisms of the so-called easy-believism and reliance on the sinners prayer et al.

</aside>

<section>

### Moment of Faith

Faith is the moment we are justified, not baptism. If baptism is, then faith is not

This is the biggest issue I have with the doctrine that baptism is the time at which one is united to christ. This _seems to_ contradict the teaching, essential to christianity, that justification is through faith alone. Specifically, this would seem to contradict the following verses. If baptism is the moment that justification happens, then these verses must be interpreted in a way that doesn't take them at their most plain meaning

{{ scripture.list([
  "Acts 10:43",
  "Acts 13:39",
  "Acts 15:9",
  "Acts 16:31",
  "Romans 1:16-17",
  "Romans 3:22-30",
  "Romans 4:1-5, 9-13",
  "Romans 4:24-5:2",
  "Romans 9:30",
  "Romans 10:6, 9-17",
  "1 Corinthians 15:2",
  "Galatians 2:16",
  "Galatians 3:2-9, 14",
  "Galatians 3:22-27",
  "Galatians 5:6",
  "Ephesians 1:13",
  "Ephesians 2:8",
  "Philippians 3:8-9",
  "2 Timothy 3:15",
  "1 John 5:1-5"
  ]) }}


</section>

<section>

### Gospel Apart from Baptism

The gospel is the power of God for salvation (Romans 1:16), and the gospel does not necessarily include baptism. Importantly, it is the gospel is hich brings about conversion

{{ scripture.list([
  "1 Corinthians 1:17-25",
  "Romans 1:16",
  "James 1:18"
  ]) }}
  
</section>

<section>

### No Baptism in the OT

The old testament has no teaching of a baptism reminiscent of the christian baptism and especially not of a baptism which brings about the forgiveness of sins. 2 Timothy 3:15 indicates that the sacred writings (referring to the ot at the time) were able to instruct timothy for salvation. therefore, baptism must not be required for salvation.

{{ scripture.list([
  "2 Timothy 3:15"
  ]) }}
  
</section>

<section>

### Baptism for Depraved People?

Before someone is born again and united to christ they are spiritually dead, cannot please God, have no hope in this world, and are slaves to sin. A non-christian would never willingly put himself in a place to receive mercy from God. They would not choose to be baptized until they are already born again, but at that point they are already born again.

{{ scripture.list([
  "Romans 8:5-9",
  "1 Corinthians 2:14"
  ]) }}
  
</section>

<section>

### Christians are Called Believers not the Baptized

This one is more of an observation that I find surprising, if baptism is really the dividing line between a true born-again Christian and everyone else. All through the New Testament christians are called believers, and are never called _the immersed_, or some such thing. To me, this implies that the belief is the crucial point in Christianity, not immersion.

</section>


## Other Issues

I have frequently seen the argument made that "every recorded example of salvation" includes baptism. **This is simply not true**. I've created a page that lists out [every conversion in Acts](./conversions-in-acts), so you can see for yourself.

One response I've had to this list is that most of those are just quick mentions, not including any details, and all the detailed ones do include baptism. However, this is only true if "detailed" means "includes baptisms". In other words, imagine if Acts 13:12 included the proconsul getting baptised. It would then be included in the list of salvation accounts which include baptism. Since it doesn't include baptism, it must be a salvation account which doesn't include baptism.