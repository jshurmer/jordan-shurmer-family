---
title: Regeneration
description: "A look into my views on regeneration."
date: 2019-02-23
templateEngine: njk,md
---
{% import "macros/scripture.njk" as scripture %}

## Introduction

<p class="summary">
What is Regeneration?
</p>

Regeneration is a spiritual change brought about by the Holy Spirit in the heart of someone. God is the sole actor in the act of regeneration; we are dead, blind, and asleep when he gives us life, sight, and wakes us up. It is a new birth, a new life, and new sight. The heart that was once cold to the things of God is woken up. What was once foolish is desirable.

In the Old Testament, regeneration was announced as part of the coming New Covenant. Regeneration is one of the differences between the Old and New Covenant.

{{ scripture.list([
  "Jeremiah 31:31-34",
  "Ezekiel 36:25-27",
  "Ezekiel 37:14",
  "Deuteronomy 30:6",
  "John 1:12-13",
  "John 3:3-8",
  "Ephesians 2:1-10",
  "1 Corinthians 1:26-31",
  "2 Corinthians 5:17",
  "Titus 3:3-7",
  "1 Peter 1:3-5",
  "1 John 3:9",
  "1 John 5:1"
  ]) }}

## Regeneration Through the Gospel

<p class="summary">
The gospel is God's ordained means to bring about the new birth
</p>

Regeneration doesn't happen randomly, or arbitrarily. The gospel is the power of God for salvation, because the Gospel is god's ordained means for bringing about regeneration. It is while someone hears or contemplates the realities of the gospel that the Holy Spirit gives the life, replaces the heart of stone, grants sight to the blind eyes, etc.

{{ scripture.list([
  "James 1:18",
  "1 Corinthians 1:17-25",
  "1 Peter 1:22-25"
  ]) }}

## Regeneration Provides Faith

Regeneration must happen to a person in order for them to believe. Prior to regeneration the depraved person is dead to the things of God: unwilling, untrusting, blind, and dead. In this condition there is no hope for fallen man on his own. God must intervene and give life where there is none.

### Without God

Our state without God is hopeless. We cannot enter the kingdom of God, we cannot understand spiritual things, we are blinded by the Devil, we are slaves to our own passions, we are hopeless. We would all remain in this state of death, blindness, and slavery if God didn't give us new life, open our eyes, and free us.

{{ scripture.list([
  "Matthew 11:27",
  "Mark 10:23-27",
  "John 3:3-8",
  "John 6:44,63-65",
  "Romans 8:5-9",
  "Ephesians 2:1-3",
  "2 Corinthians 4:3-4",
  "Titus 3:3"
  ]) }}

### God gives life

God, the author of life, steps in by his Spirit through the gospel and provides life. Where there was death he provides life; where there was blindness he opens eyes; he frees slaves of sin; the foolish are made wise; enemies are made sons.

{{ scripture.list([
  "Matthew 16:17",
  "John 6:44-45",
  "Acts 16:14",
  "Ephesians 2:1-10",
  "2 Corinthians 4:3-7",
  "2 Timothy 1:8-10",
  "Titus 3:3-7",
  "1 John 5:1"
  ]) }}
