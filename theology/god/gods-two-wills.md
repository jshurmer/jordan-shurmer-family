---
title: God's Two Wills
description: A look into my view of the so-called \"two wills\" of God
draft: true
templateEngine: njk,md
---

{% import "macros/scripture.njk" as scripture %}

## Introduction

{{ scripture.list([
    "Lamentations 3:32-33"
    ]) }}
    
## Peter's two uses

{{ scripture.list([
    "1 Peter 2:15, 4:2",
    "1 Peter 3:17, 4:19"
    ]) }}

## Examples

* Abraham sacrificing Isaac
* Hardening Pharaoh's heart
* Egyptians hating the Jews (Psalm 105:25)
* Evil against David (2 Samuel 12:11)
* The death of Christ
* The war against the lamb (Revelation 17:16-17)

{{ scripture.list([
    "Exodus 4:21",
    "Deuteronomy 2:30",
    "Deuteronomy 28:63",
    "Joshua 11:19-20",
    "1 Samuel 2:25",
    "2 Samuel 12:11",
    "Psalm 105:25",
    "Amos 3:6",
    "Romans 11:7-9, 25-26",
    "Revelation 17:16-17"
    ]) }}
    






## Resources

* John Piper: [Are there two wills in God](https://www.desiringgod.org/articles/are-there-two-wills-in-god)

