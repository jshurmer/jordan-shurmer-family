---
title: Sovereignty
description: "A look into my view of God's sovereignty"
date: 2019-02-20
headline: God's Sovereignty
templateEngine: njk,md
---
{% import "macros/scripture.njk" as scripture %}

<section>

## Introduction

<p class="summary">
God's sovereignty is his ultimate control over all things. He does all that he pleases. Nothing happens that he has not brought about. Everything he does has a purpose.

<q>**Then Job answered the LORD and said: "I know that you can do all things, and that no purpose of yours can be thwarted."**</q>
</p>

This is a grand topic, which we will never fully understand. Our minds are finite and small while God is infinite, eternal, immense, and he has chosen to hide many things from us. Yet, he has also revealed many things to us about His power and His activity in the world. As we humbly read His word we can learn amazing truths about the reality he has created.

Seeing God for who he is is not merely an intellectual exercise. These things cause tremendous changes in our lives and the way we relate to God as we grow to understand them.

* Our amazement and worship of God increases
* Our trust in God grows, and our prayers to him increase
* Our confidence in God causes us to be courageous and bold

</section>

<aside>

## Man's Responsibility

As we learn about God's sovereignty questions about our individual responsibility will arise. While that topic deserves it's own study, a brief look suffices to hold ourselves morally responsible, even while God is sovereign. How these things can both be may be a mystery at times, but the scripture upholds both as true and so must we. God's sovereignty and Man's moral responsibility are compatible.

{{ scripture.list([
  "Jeremiah 17:10",
  "Romans 2:6-8",
  "Romans 14:12"
  ]) }}

</aside>

<section>

## God's Universal Sovereignty

<p class="summary">
God's rule extends to all creation
</p>

God is creator, sustainer, and author of all things. Nothing is outside of his oversight and presence. This includes all humanity, every plant and animal, all planets known and unkown to us, all stars and nebulae, every angel and devil, everything that exists.

{{ scripture.list([
  "Genesis 18:14",
  "2 Chronicles 20:6",
  "Job 34:14-15",
  "Job 41:11",
  "Job 42:1-2",
  "Jeremiah 32:27",
  "Matthew 6:26-28",
  "Matthew 10:29-30",
  "Mark 14:36",
  "Luke 1:37",
  "Acts 17:24-25",
  "Romans 8:28",
  "Ephesians 1:11",
  "Colossians 1:15-17",
  "Philippians 3:21",
  "Hebrews 1:3",
  "2 Peter 3:7"
  ]) }}
  
</section>

<section>

## God's Effective Sovereignty

<p class="summary">
God is sovereign _de facto_. He doesn't merely hold power, He exercises his power.
</p>

Everything that happens happens because he has willed it. He ordains every event in history, and he has a purpose for all of them. He works all things according to his good pleasure and all things work according to his will.

{{ scripture.list([
  "Job 42:1-2",
  "Psalm 33:10-11",
  "Psalm 115:1-3",
  "Psalm 135:5-6",
  "Proverbs 16:9",
  "Proverbs 16:33",
  "Proverbs 19:21",
  "Proverbs 21:30",
  "Isaiah 14:24-27",
  "Isaiah 43:11-13",
  "Isaiah 46:9-10",
  "Isaiah 55:11",
  "Lamentations 3:37-38",
  "Daniel 4:35",
  "Ephesians 1:11"
  ]) }}
  
</section>

<section>

## Sovereign Over Nature

<p class="summary">
God exercises total control over all of nature.
</p>

Everything in nature happens due to God's activity and involvement. Animals are born and fed by him; snow storms, rain storms, ice storms, and hurricanes are all his acts; stars exist and continue their nuclear fusion according to his will; water to drink, food to eat, sunrises and sunsets, dew, frost, and fog all come from his hand as do earthquakes, famine, blight, and disease.

God is not watching things play out as a distant ruler. He is not like a clockmaker who wound up the Universe at the beginning and then leaves it be to function as he planned. He is intimately involved in every aspect of creation and _natural laws_.

{{ scripture.list([
  "Job 37",
  "Job 38",
  "Psalm 65:9-11",
  "Psalm 104",
  "Psalm 107:23-43",
  "Psalm 135:5-7",
  "Psalm 136:25-26",
  "Psalm 145:15-16",
  "Psalm 147",
  "Jeremiah 10:12-13"
  ]) }}
  
</section>

<section>

## Sovereign Over History

<p class="summary">
God orchestrates history for his purposes. By his command nations, kingdom, and empires rise and fall, treaties are signed and voided, wars are won and lost.
</p>

History is God's story being played out in real time. This includes every event whether good or evil. God is not the author of human sin, but it is part of his plan. He does not simply react to bad events after the fact; rather, he has planned them from the beginning, to ultimately bring him more glory. God doesn't only use evil for good, he plans it for good.

{{ scripture.list([
  "Genesis 50:20"
  ]) }}

God moves history forward through the actions of many people, but it is ultimately God nonetheless. For example, military victory is brought about only by God's will. Think of how much history this includes.

{{ scripture.list([
  "Deuteronomy 20:1-15",
  "Joshua 21:43-45",
  "Joshua 24:11",
  "Judges 7:1-23",
  "1 Samuel 17:46-47",
  "2 Chronicles 20:15",
  "Proverbs 21:31",
  "Zechariah 4:6",
  "Isaiah 10:5-12",
  "Isaiah 14:24-25",
  "Habakkuk 1:5-12",
  "Micah 4:12"
  ]) }}

After being in exile for years, it was time for God's people to return home. God ensure that his people were able to get back to Israel. He used Syria and Persia, conquering Babylon, and gave Israel favor with their leaders.

{{ scripture.list([
  "Ezra 1:1-4",
  "Ezra 6:22",
  "Ezra 7:27",
  "Isaiah 44:28",
  "Isaiah 45:1,5-7",
  "Jeremiah 29:11-14",
  "Jeremiah 30"
  ]) }}

At the turning point of history, the life and death of Jesus, we see God's sovereignty at work more clearly than ever!

{{ scripture.list([
  "Acts 2:23",
  "Acts 3:18",
  "Acts 4:27-28",
  "Luke 22:22",
  "Matthew 26:4",
  "Mark 14:21",
  "John 2:4",
  "John 7:6,30,44",
  "John 8:20",
  "John 12:23,27",
  "John 13:1",
  "John 16:21",
  "John 17:1"
  ]) }}
  
</section>

<section>

## Sovereign over Individual Lives

<p class="summary">
God rules over our lives
</p>

The fact that he rules all of history implies that he also governs individual lives. How else does he accomplish the history shaping other than through the lives of individuals. However, we're not left to make that inference, we have scripture that are explicit about God's sovereignty in relation to individual live, like you and me.

{{ scripture.list([
  "Jeremiah 1:5",
  "Psalm 139:13-16",
  "1 Samuel 2:6-7",
  "Psalm 37:23",
  "James 4:13-16",
  "Jeremiah 10:23",
  "Genesis 41:16,28,32",
  "Genesis 45:5-8",
  "Romans 8:28"
  ]) }}

Additionally, consider the _Spiritual Gifts_. These are an integral part of who we are. As christians they form our personality, our decisions, and or interactions with others. These gifts are given out by God according to his good pleasure. They are not merely personality traits and not merely the result of our own decisions, they are also _gifts_ from our Father.

{{ scripture.list([
  "Romans 12:3-6",
  "1 Corinthians 12:4-11"
  ]) }}

</section>

<section>

## Sovereign over Human Decisions

<p class="summary">
God governs our decisions, ensuring that his purposes are accomplished in our own decisions
</p>

Humans are [responsible moral agents,](#responsibility) but we must acknowledge that our decisions are not independent of God. God's purposes stand behind the free decisions of mankind. This can be inferred from the previous claims about His governing of history and individual lives, but we can call out some explicit verses on this more narrow topic as well.
</p>

{{ scripture.list([
  "Genesis 45:5-8",
  "Exodus 3:21",
  "Exodus 12:36",
  "Judges 7:22",
  "Proverbs 19:21",
  "Isaiah 44:28",
  "Daniel 1:9"
  ]) }}

### The Human Heart

The good person out of the good treasure in his heart produces good, and the evil person out of his evil treasure produces evil, for out of the abundance of the heart the mouth speaks. Our decisions come from our hearts, and yet God influences and governs the hear.

{{ scripture.list([
  "Proverbs 16:9",
  "Proverbs 21:1",
  "Psalm 105:24-25",
  "Deuteronomy 2:30",
  "Joshua 11:18-20",
  "1 Samuel 2:25",
  "1 Samuel 10:9",
  "2 Chronicles 25:20",
  "Ezra 6:22"
  ]) }}

#### Pharaoh in the Exodus Story

Pharaoh's experience in the Exodus story is a clear example of God governing the human heart for his purposes.

{{ scripture.list([
  "Exodus 4:21",
  "Exodus 7:3-4",
  "Exodus 7:13",
  "Exodus 7:22",
  "Exodus 8:19",
  "Exodus 9:12",
  "Exodus 9:35",
  "Exodus 10:1",
  "Exodus 10:20",
  "Exodus 10:27",
  "Exodus 11:10",
  "Exodus 14:4",
  "Exodus 14:8",
  "Romans 9:17-18"
  ]) }}
  
</section>

<section>

## Sovereign over Sin

<p class="summary">
God governs human sin in a way that does not cause him to sin.
</p>

We are getting more into the mystery of God's will. He is able to govern and ordain our sin, and yet he remains pure and sinless. There have been many attempts to explain how this can be, some of them more helpful than others, yet ultimately we must turn to God's revelation for our understanding and affirm what it affirms.

Some people speak of God's _Decretive_ will and God's _Perceptive_ will, or His _Will of Decree_ and _Will of Command_, or his _Sovereign Will_ and _Moral Will_. His Decretive or Sovereign will would be what He, in his infinite wisdom, decrees to happen whether good or bad. His decrees must come to pass, but it is not as though He can be charged with wrong doing. This would include what people commonly refer to as God's Providence and all the things He Ordains to happen. His Perceptive or Moral will, on the other hand, is his revealed desire for how we are intended to act. More on this in my notes on [God's two wills](gods-two-wills). For the sake of this topic we will leave the explanation of God's two will aside, and focus on what scripture reveals, rather than how to make sense of potentially contradicting things.

Scripture makes it plain that God is in control of the human heart (see [above](#human-decisions)), which is also deceitful above all things and desperately sick. God gives people over to their sin, makes people wander from his ways, uses wicked nations as his rod of anger, incites Kings to disobedience, sends people through slavery, and plans the unjust death of an innocent man.

{{ scripture.list([
  "2 Samuel 17:14",
  "1 Kings 12:15",
  "1 Kings 22:20-23",
  "Proverbs 16:4",
  "Jeremiah 17:9",
  "Jeremiah 32:42",
  "Isaiah 10:5-15",
  "Isaiah 63:17",
  "Romans 1:24-32",
  "Revelation 17:17"
  ]) }}

### David's Census

Incited by God, incited by Satan, carried out by David, and David is accountable.

{{ scripture.list([
  "2 Samuel 24:1",
  "2 Samuel 24:10",
  "1 Chronicles 21:1",
  "1 Chronicles 21:7-8"
  ]) }}

### Joseph's Life

Sold into slavery by his brothers, yet at the end of his life gives the credit to God: _it was not you who sent me here, but God._

{{ scripture.list([
  "Genesis 45:5-8",
  "Genesis 50:20"
  ]) }}

### Jesus's Life

So much of Jesus's life and death involved sin, and it was all planned by God.

{{ scripture.list([
  "Luke 22:22",
  "Acts 2:23-24",
  "Acts 3:18",
  "Acts 4:27-28"
  ]) }}

We can see that God is at work in these sinful acts. God does not have his hands tied, even by his own choice. God's purposes do not fail to come about due to sin, rather God's purposes succeed through sin. This is an amazing doctrine that can sustain Christians through hard times, persecutions, and life in a fallen world. God is never out of control.

</section>

<section>

## Sovereign in Salvation

<p class="summary">
God's sovereign good pleasure is the source and driving force behind everyone's salvation
</p>

More on this topic in my notes on election, but suffice it to say that God is the primary actor in our salvation. Praise God!

{{ scripture.list([
  "Zechariah 12:10",
  "Ezekiel 11:19-20",
  "John 6:44",
  "Ephesians 2:9-10",
  "2 Timothy 1:9",
  "2 Timothy 2:24-25",
  "James 1:18"
  ]) }}
  
</section>

<section>

## Resources 

### Online:

* John Piper: [Are there two wills in God](https://www.desiringgod.org/articles/are-there-two-wills-in-god)

### Music:

* William Cowper: [God Moves in a Mysterious Way](https://en.wikipedia.org/wiki/God_Moves_in_a_Mysterious_Way)

</section>

