---
title: Development
layout: layouts/index-page.njk
---

Dev Notes
===========

Some of my notes, thoughts, and experiments in the world of software engineering.


Books
-----

* [The Pragmatic Programmer](./pragmatic-programmer)

All Dev Pages
-------------
