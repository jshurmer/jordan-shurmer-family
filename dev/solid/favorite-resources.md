---
title: Solid Resources
---

Solid Resources
===============

Here are some of my favorite resources about the Solid Project.

- Ruben Verborgh's [Blog](https://ruben.verborgh.org/blog/)
  - [Paradigm shifts for the decentralized Web](https://ruben.verborgh.org/blog/2017/12/20/paradigm-shifts-for-the-decentralized-web/)