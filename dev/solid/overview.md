---
title: "Solid Project - My Thoughts"
---

Solid Overview
==============

This is my personal overview about what Solid is, why I think it's important, and other observations from interacting with the solid community.

What it is
----------

The name **Solid** originally came from "**So**cial **Li**nked **D**ata", but it seems like the Solid community has given up on that acronym and most treat Solid as just a stand alone name.

The goal of the Solid project is to re-decentralize the web. The current _pre-solid_ web is primarily made up of corporate companies collecting user data and selling it to advertisers. The data that each website has about you, the user, is owned by that website in the sense that you gave the information over to them for them to use how they see fit. If the website is _good_ they will let you manage that data (update things, remove things, etc), but for all practical purposes the data is now theirs. Their have been legal restrictions put in place to curtail some of the more malicious things that companies can do with this, but that is still the general paradigm for the web. If you want to use Facebook you give them your information, if you want to use YouTube you give them your information, Amazon, Netflix, Reddit, Instagram, Github, etc. etc. etc. all require you to give them your information in order to fully utilize their services. You then have to maintain that information separately in each website, and they get to use it however they see fit.

Solid provides a total paradigm shift. In the Solid web your information is owned by you directly, and managed by you directly. Websites that want to know something about you can look it up when they need it and when you give them permission. No need to re-enter your information on every website, no need to maintain it on every website. Your data is yours.

More to come
------------

Stay tuned as I continue to fill in my thoughts on Solid.