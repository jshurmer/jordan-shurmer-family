---
title: Solid Project
layout: layouts/index-page.njk
---

SoLiD
=====

This section is all about [solid]. The project aiming to re-decentralize the web by giving users control of their own data.



[solid]: https://solidproject.org/