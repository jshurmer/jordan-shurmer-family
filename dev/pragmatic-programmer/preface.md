---
title: "Pragmatic Programmer: Preface"
---

# The Pragmatic Programmer: Preface

> This book will help you become a better programmer

A good start to the book. Seems to be realistic - not promising easy answers or simple tricks, but instead providing guiding thoughts and tips to remember. They also acknowledge that each developer is unique, and each situation is unique. This book is not intended to tell you _how to be a developer_, and print out a copy of every other _pragmatic programmer_


So, what is a pragmatic programmer? Someone who shares many of the following characteristics:

* Early adopter/fast adapter
* Inquisitive
* Critical thinker
* Realistic
* Jack of all trades
* Care about your craft
* "Think!" about your work

I like this list. _Early adopter_ surprises me.. I guess it's important to be able to grasp new things quickly and adapt to changing situations. The rest all make immediate sense to me. _Realistic_ is probably the one I have the toughest time with.

The preface ends with two very good thoughts:

1) The necessity of individual and teams. Each individual should have their own sense of responsibility and individuality in crafting the software. And the team should function as a disciplined and cohesive unit.

2) It's a continuous process. Don't expect immediate perfection, instead aim for _kaizen_ - continual small improvements

## Tips

* Care about your craft
* Think! about your work
