---
title: The Pragmatic Programmer
layout: layouts/index-page.njk
---

The Pragmatic Programmer
========================

> your journey to mastery

Well, here's my journey.


Getting Started
---------------

I heard about this book while researching good resources for along the lines of "software craftsmanship". I'm not familiar with the authors at all. It seems like they were involved in the original Agile creation, and other movements to bring good change to the world of software development.

After reading the back of the book and the recommendations I'm excited to get started.

Things I'm expecting:

* Principles to help guide my decisions as a software dev
  * for interacting with my team, with stakeholders, etc
  * for writing good software
* Tips for interacting with "techincal debt"
* encouragements to continually learn
* Good ways of thinking about the whole dev process
* Mostly prose, rather than coding excercises

Alright, lets jump in
