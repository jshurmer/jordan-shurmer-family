---
title: Hi Ren
wip: true
---

[the video]: https://www.youtube.com/watch?v=tsmPCi7NKrg

Watch [the video]

This is a video, and a song, made for his fans.

NF is declaring, visually and lyrically, that a new phase of NF music is coming.
Fear is no longer in control, Hope is taking the reins.

The NF dressed in white is _Hope_, the new NF. The NF dressed in black is _Fear_, the old NF.
Watch [the video] again with this in mind. Hope is drifting in the ocean, but touches down as 
the song gets started. He immeditealy encounters Fear, who seems to be helping him find his way.
However, Fear is just leading him in circles back to where he started, even pushing him back into
his Mansion.

Hope goes through some of the rooms that Fear has set up. Dealing with the same thoughts that Fear has already dealt with, but from
a hopeful point of view.
The burdens, mental breakdowns, and trauma are now a part of what made him who he is.

Hope then details 31 ways that Fear has been leading his life in the wrong way. Fear tries to fight back, but Hope puts him in his place with the final two lines:

> Hope: I know I can change ....
> Fear: you don't have the guts
> Hope: you're the one afraid
> Fear: I'm the one in charge
> HOpe: I'm taking the reins
