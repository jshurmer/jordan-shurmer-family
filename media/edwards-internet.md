---
title: Jonathan Edwards Predicts the Internet
---

Thanks to [Matthew Everhard's tweet](https://twitter.com/matt_everhard/status/1654111305232465920) for pointing out this really interesting writing from Edward's Miscellanious writings (Miscellanies No. 262):

> 'Tis probable that this world shall be more like heaven in the millennium in this respect, that contemplative and spiritual employments, and those things that more directly concern the mind and religion, will be more the saints' ordinary business than now. There will be so many contrivances and inventions to facilitate and expedite their necessary secular business, that they shall have more time for noble exercises, and that they will have better contrivances for assisting one another through the whole earth, by a mere expedite and easy and safe communication between distant regions than now.


You can read this on page 42 of [the original](https://collections.library.yale.edu/catalog/2065879) (near the top, starting with _Millennium_), if you can manage to read his writing. I coped the text above from Matthew Everhard's [Youtube video](https://www.youtube.com/watch?v=AH213SXhOGU).

![Original Script of Misc 262](/static/img/edwards-internet-og.png)

Edwards is saying that he thinks society will continue to progress, with new inventions and technology, to a point where global communication will be easy and safe. He essentially ties this to God's work of building his kingdom, equipping the saints to focus on things that "directly concern their mind and religion". In his view this is making our societies "more like heaven"!

The internet certainly fulfills this brief description from Edwards. A _contrivance for assisting one another through the whole earth, by a amere expedite and easy and safe communication between distant regions_. Maybe there'll be some future iteration of tech that even more fully realize this vision, especially the _safe_ part.


## Postmillennialism

It seems to me that this sort of _hopeful_ predictions about human flourishing is a direct result of Edwards' [postmil theology](https://www.monergism.com/topics/eschatology/all-millennial-views/postmillennialism). His assumption is that God will be building His kingdom over a period of time, and using his Church and his people to do it. This is not an abstract spiritual reality, but a practical one which requires "easy and safe communication between distant regions".

It reminds me of some recent stuff I've heard from Doug Wilson. In [Ploductivity](https://canonpress.com/products/ploductivity/) Wilson briefly lays out his Theology of Technology. He has a positive view of modern technology, and helpfully challenges Christians to consider how effectively and how wisely they can use it.

> If a smart phone is like owning 10,000 servants are 9,999 of yours out on the veranda smoking?

Wilson is also a Postmillennialist, and it's easy to see the shared positivity and hopefulness about practical human endeavors that he and Edwards have. 
