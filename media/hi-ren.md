---
title: Hi Ren
wip: true
---

Watch [the video](https://www.youtube.com/watch?v=s_nc1IVoMxc)

Then, watch [the video](https://www.youtube.com/watch?v=s_nc1IVoMxc) twice. 

5 parts

1. the dance
2. the battle
3. fear and hope
4. the dance again
5. the monologue



In the monoluge Ren explains his view on the old battle of good vs evil. He says that he used to think of it like a battle, like david vs goliath, inside himself. Now he views it more like a dance.

You can see these ideas play out through thte course of the song.

The falcetto voice represents him embracing the "dance" aspect.

The lyrical part shows the battle aspect. The good voice starts calm and clear, countering every criticism with its own valid remarks. As they progress, however, the calm voice becomes more intense and ends up loosing all control with descriptive threats of death and lossing the ability to speak clearly.