---
title: The Liberal Arts Tradition
wip: true
---

Great overview of what Classical Education is.

In particular, the chapter about Musical Education. Learning from a place of wonder rather than criticism. 

> He that breaks a thing to find out what it is has left the path of wisdom &mdash; J.R.R. Tolkien